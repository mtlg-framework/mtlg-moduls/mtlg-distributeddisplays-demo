/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-05-30T16:32:22+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2017-10-08T19:01:33+02:00
 */



/*global module:false*/
module.exports = function(grunt) {

  var path = require('path');

  require(path.join(process.cwd(), 'node_modules/MTLG/devGrunt/initConfig.js'))(grunt);
  require(path.join(process.cwd(), 'node_modules/MTLG/devGrunt/loadNPMTasks.js'))(grunt);
  require(path.join(process.cwd(), 'node_modules/MTLG/devGrunt/registerTasks.js'))(grunt);


};
