/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2017-11-13T17:54:41+01:00
 */



/*
* manages drawing into playing fields of every area
*/

/**
 * Initialize the first field: Room selection
 */
var feld1_init = function () {
    if (!demo.initialized) {
        // Setup the customActions to request the sharedRoom object
        MTLG.distributedDisplays.actions.setCustomFunction("getSharedRoom", customActionGetSharedRoom);
        MTLG.distributedDisplays.actions.setCustomFunction("receiveSharedRoom", customActionReceiveSharedRoom);
        // Setup customFunction to react to updates
        MTLG.distributedDisplays.actions.registerListenerForAction("updateSharedObject", customFunctionOnUpdate);
        demo.debug = true;
        demo.initialized = true;
    }

    stage.removeAllChildren();
    demo.roomName = "";
    demo.sharedRoomObject = null;
    demo.refreshFunction = refreshRoomList;
    demo.refreshInterval = 1000;//ms
    demo.nextRefresh = demo.refreshInterval;
    demo.w = MTLG.getOptions().width / 100.0;
    demo.h = MTLG.getOptions().height / 100.0;
    demo.font = "px Helvetica";
    demo.textColor = "#333";
    demo.disabledColor = "#DDD";
    demo.disabledBackgroundColor = "#EEE";
    demo.noObstacleColor = "#F3F8FE"; // Almost white
    demo.buttonBackgroundColor = "#9DC2F6"; // Light
    demo.characterColor = "#619EF3"; // Base
    demo.selectedColor = "#2B7FF2"; // Slightly darker
    // dark="#0367F0"
    demo.obstacleColor = "#162437"; // Other even darker
    demo.transparentColor = "rgba(255, 255, 255, 0.01)";
    demo.titleFont = Math.ceil(3 * demo.h) + demo.font;
    demo.normalFont = Math.ceil(2 * demo.h) + demo.font;
    demo.buttonFont = "bold " + demo.normalFont;
    demo.buttonMargin = 30 + demo.w;//px
    drawField1(MTLG.getOptions().width, MTLG.getOptions().height);
};

/**
 * Draw the first field: Room selection
 */
var drawField1 = function () {
    var w = demo.w;
    var h = demo.h;


    // Background
    var background = new createjs.Shape();
    background.graphics.beginFill("#FFF").drawRect(0, 0, 100 * w, 100 * h);

    // Text
    var title = new createjs.Text(locText("field1_title"), demo.titleFont, demo.textColor);
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 50 * w;
    title.y = 10 * h;

    // Rooms

    // Room selection wrapper
    var roomSelection = new createjs.Container();
    roomSelection.x = 25 * w;
    roomSelection.y = 15 * h;

    // Refresh rooms button
    var refreshRoomsButton = createButton(3 * h, locText("field1_refreshButton")); // x = y = 0
    refreshRoomsButton.on('click', refreshRoomList);

    // Create new room button
    var newRoomButton = createButton(3 * h, locText("field1_newRoomButton"));
    newRoomButton.x = refreshRoomsButton.getBounds().width + demo.buttonMargin;
    newRoomButton.on('click', addRoom);

    // Room list
    demo.field1.roomList = new createjs.Container();
    demo.field1.roomList.y = 6 * h;

    roomSelection.addChild(refreshRoomsButton);
    roomSelection.addChild(newRoomButton);
    roomSelection.addChild(demo.field1.roomList);

    stage.addChild(background);
    stage.addChild(title);
    stage.addChild(roomSelection);
    drawFullscreenButton();
    stage.update();
};


/**
 * Handler for the press of the control selection buttons
 * @param {object} event The click event
 * @param {string} control The name of the clicked control
 */
var controlClickedHandler = function (event, control) {
    var id = MTLG.distributedDisplays.rooms.getClientId();
    var controls = demo.sharedRoom.controls;
    if (controls[control]) {
        // Control is assigned, if to self then unassign, otherwise noop
        if (controls[control] === id) {
            controls[control] = false; // Unassign control
            demo.field2.controlButtons[control].changeState(demo.textColor, "");
        }
    } else {
        // Control is not assigned, assign to self
        var existing = false;
        for (var c in controls) {
            if (c === control) existing = true;
            if (controls[c] === id) {
                controls[c] = false; // Unassing other selected control
                demo.field2.controlButtons[c].changeState(demo.textColor, "");
            }
        }
        // Assing new control if existing
        if (existing) {
            controls[control] = id;
            demo.field2.controlButtons[control].changeState(demo.selectedColor, id);
        }
    }
};

/**
 * Refreshes the members of the joined room
 */
var refreshMemberList = function () {
    MTLG.distributedDisplays.rooms.getJoinedRooms(function (result) {
        if (result && !(result.success === false)) {
            if (result[demo.roomName]) {
                demo.members = result[demo.roomName].sockets;
                demo.controlling = false;
                var id = MTLG.distributedDisplays.rooms.getClientId();

                if (demo.sharedRoom) {
                    var controls = demo.sharedRoom.controls;
                    for (var i in controls) {
                        if (demo.field2.controlButtons) demo.field2.controlButtons[i].changeState(demo.textColor, ""); // If is required for a late response after switching to game

                        // For each control, add the info to the member who controls it
                        if (controls[i]) { // If the control is assigned to a player
                            if (demo.members[controls[i]]) {
                                demo.members[controls[i]] = i; // If that player is a member, save the control info for that member
                                var color = demo.disabledColor;
                                if (controls[i] === id) {
                                    color = demo.selectedColor;
                                    demo.controlling = i;
                                }
                                if (demo.field2.controlButtons) demo.field2.controlButtons[i].changeState(color, controls[i]); // Show that a player is assigned to this control
                            } else {
                                demo.sharedRoom.controls[i] = false; // Remove the assigned player from that control, since he is not a member of this room
                            }
                        }
                    }
                } else { // No sharedRoom object
                    if (result[demo.roomName].length <= 1 && demo.members[id]) {
                        // This client is the only client in the room, so a new sharedRoom is created
                        demo.sharedRoom = createSharedRoomObject(demo.roomName);
                    } else {
                        // Request the sharedRoom object from the other members
                        MTLG.distributedDisplays.communication.sendCustomAction(demo.roomName, "getSharedRoom", id, demo.roomName);
                    }
                }

                if (demo.field2.memberList) {
                    // Display the member list
                    demo.field2.memberList.removeAllChildren();
                    var y = 0;
                    for (let memberName in demo.members) {
                        var text = new createjs.Text(memberName + ': ' + locText("field2_memberControls_" + demo.members[memberName] ), demo.normalFont, demo.textColor);
                        text.y = y;
                        demo.field2.memberList.addChild(text);
                        y += 4 * demo.h;
                    }

                    // Enable / disable the start button
                    if (demo.controlling) {
                        demo.field2.startButton.children[0].graphics._fill.style = demo.buttonBackgroundColor;
                        demo.field2.startButton.children[1].color = demo.textColor;
                    } else {
                        demo.field2.startButton.children[0].graphics._fill.style = demo.disabledBackgroundColor;
                        demo.field2.startButton.children[1].color = demo.disabledColor;
                    }
                }
            }
        }
    });
};
