/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2017-11-13T17:11:34+01:00
 */



var drawMainMenu = function(){

  var stage = MTLG.getStage();

  //Check if this is the first time the menu is created
  //In this case we also create an overlay menu
  if(firstRun){
    firstRun = false;
    //Create Menu
    MTLG.menu.addSubMenu(function(){
      console.log("call game submenu");
    });
    MTLG.menu.start();
  }

  //We can check if there are already some players present
  //In this case we just log them all out to start again
  //Alternatively you could add an option to start the game with the same players
  if(MTLG.getPlayerNumber() >= 1){
    //Remove players from MTLG
    MTLG.removeAllPlayers();
    //Remove local reference
    players = [];
  }


  //Draw the main menu
  //Text
//  var template = l("demo_game");
  var variableTextSize = 50;
  var title = new createjs.Text('AAAA', variableTextSize + 'px Arial', 'black');
  var coordinates = {xCoord : options.width / 2, yCoord : options.height/4};
  title.x = coordinates.xCoord;
  title.y = coordinates.yCoord;
  title.regX = title.getBounds().width/2;
  title.regY = title.getBounds().height/2;
  title.textBaseline = 'alphabetic';

  //Start
//  var templateStart = l("start!");
  variableTextSize = 30;
  var startText = new createjs.Text("STart", variableTextSize + 'px Arial', 'black');
  coordinates = {xCoord : options.width / 2, yCoord : options.height/2};
  startText.x = coordinates.xCoord;
  startText.y = coordinates.yCoord;
  startText.regX = startText.getBounds().width/2;
  startText.regY = 0; //startText.getBounds().height;
  startText.textBaseline = 'alphabetic';

  //White Background Rectangle
  var background = new createjs.Shape();
  background.graphics.beginFill('white').drawRect(0, 0, options.width, options.height);
  coordinates = {xCoord : options.width / 2, yCoord : options.height/2};
  background.regX = options.width / 2;
  background.regY = options.height / 2;
  background.x = coordinates.xCoord;
  background.y = coordinates.yCoord;


  //Button
  var button = new createjs.Shape();
  var width = (options.width * 0.1);
  button.graphics.beginStroke('red').beginFill('white').drawRect(0, 0, width, 70);
  coordinates = {xCoord : options.width / 2, yCoord : options.height/2};
  button.regX = width / 2;
  button.regY = 35;
  button.x = coordinates.xCoord;
  button.y = coordinates.yCoord;
  button.on('click', function(evt){
    MTLG.lc.goToLogin(); //Leave Main Menu and go to login
  });


  stage.addChild(background);
  stage.addChild(button);
  stage.addChild(title);
  stage.addChild(startText);

  //Sound Demo
  //Play the "Welcome" Sound first, then start the Background music
  //The "Welcome" Sound is a language sound, so it will play in the current language
  var soundInstance = MTLG.assets.playLangSound("mainMenu/hello");
  soundInstance.on("complete", function(){
    MTLG.assets.playBackgroundMusic("bgm/the_field_of_dreams");
    MTLG.assets.setBackgroundMusicVolume(0.5);
  }, this);
}
