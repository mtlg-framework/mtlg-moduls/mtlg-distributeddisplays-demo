
var drawFeedback = function()
{
    var stage = MTLG.getStage();

    var background = new createjs.Shape();
    background.graphics.beginFill('green').drawRect(0, 0, options.width, options.height);

    var results = [];
    var min = Number.MAX_VALUE;
    var minIndex = areas.length;
    var texts = [];

    for(var i = 0; i < areas.length; i++){
        results[i] = (tend[i] - tstart[i]);
        if(results[i] <= min){
            min = results[i];
            minIndex = i;
        }
    }

    for(var j = 0; j < areas.length; j++){
        //extend text variable to enbale number formatting
        var number = results[j];
        var text = areas[j].player.name + ': ' + number +  l("feedback_text");
        texts[j] = new createjs.Text(text, '30px Arial', 'black');
    }

    stage.addChild(background);

    for(var k = 0; k < areas.length; k++){
        texts[minIndex].x = 100;
        texts[minIndex].y = 100 + (k*100);
        stage.addChild(texts[minIndex]);
        if(minIndex === 0){
            minIndex = 1;
        }
        else {
            minIndex = 0;
        }
    }

  //Return to Menu button text
  var templateStart = l("return_to_menu");
  variableTextSize = 30;
  var startText = new createjs.Text(templateStart, variableTextSize + 'px Arial', 'black');
  coordinates  = {xCoord : options.width * 4 / 5 , yCoord : options.height * 4 / 5};
  startText.x = coordinates .xCoord;
  startText.y = coordinates .yCoord;
  startText.regX = startText.getBounds().width/2;
  startText.regY = 0; //startText.getBounds().height;
  startText.textBaseline = 'alphabetic';

  //Button
  var button = new createjs.Shape();
  var width = (startText.getBounds().width + 10);
  button.graphics.beginStroke('red').beginFill('white').drawRect(0, 0, width, 70);
  coordinates  = {xCoord : options.width * 4 / 5, yCoord : options.height * 4 / 5};
  button.regX = width / 2;
  button.regY = 35;
  button.x = coordinates .xCoord;
  button.y = coordinates .yCoord;
  button.on('click', function(evt){
    MTLG.lc.goToMenu(); //Return to Main Menu
  });

  stage.addChild(button);
  stage.addChild(startText);

};


/**
 * Function that checks if feedback should be created next.
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkMyFeedback = function(gameState){
  if(!gameState){
    console.log("gameState undefined");
    return 0; //assuming we should not start
  }
  if(!gameState.done){
    console.log("gameState.done not set");
    console.log(gameState);
    return 0; //assuming we should not start
  }
  if(gameState.done === 1){
    return 1;
  } else {
    return 0;
  }
}
