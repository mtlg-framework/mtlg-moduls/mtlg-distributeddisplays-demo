/**
 * @Date:   2017-11-13T16:34:04+01:00
 * @Last modified time: 2017-11-13T16:38:27+01:00
 */



/**
 * Initialize the third field: Game
 */
var feld3_init = function () {
    demo.field3.velocityX = 0;
    demo.field3.velocityY = 0;
    demo.field3.maxVelocity = 1 / 400;

    stage.removeAllChildren();

    drawField3();

    demo.refreshFunction = moveCharacter;
    demo.refreshInterval = 0;//ms
    demo.nextRefresh = demo.refreshInterval;

    if (demo.debug) {
        demo.keyMaster = false;
        demo.keyMap = {};
        var updateKeyMap = function (e) {
            e = e || event; // to deal with IE
            demo.keyMap[e.key] = e.type === "keydown";
            if (e.type === "keydown") demo.keyMaster = true;
        }
        window.addEventListener("keydown", updateKeyMap);
        window.addEventListener("keyup", updateKeyMap);
    }
};

/**
 * Draw the third field: Game
 */
var drawField3 = function () {
    var w = demo.w;
    var h = demo.h;


    // Background
    var background = new createjs.Shape();
    background.graphics.beginFill(demo.obstacleColor).drawRect(0, 0, 100 * w, 100 * h);

    // Back to control selection
    var backToSelectControlsButton = createButton(4 * h, locText("field3_backToSelectControlsButton"));
    backToSelectControlsButton.x = h;
    backToSelectControlsButton.y = 5 * h;
    backToSelectControlsButton.on('click', backToSelectControls);


    // Scaling

    // Container for scaling, children can be attached with absolute positon and scale, they are scaled and positioned by this parent without affecting their properties
    var scaledContainer = new createjs.Container();
    scaledContainer.x = 50 * w;
    scaledContainer.y = 50 * h;
    scaledContainer.scaleX = w;
    scaledContainer.scaleY = w;


    // Control

    // Container
    demo.field3.controlContainer = new createjs.Container();
    demo.field3.controlContainer.x = 35;
    demo.field3.controlContainer.y = 28 * h / w;

    // Control element
    var controlBackground = new createjs.Shape();
    demo.field3.controlButton = new createjs.Shape();
    switch (demo.controlling) {
        case "x":
            controlBackground.graphics.beginFill(demo.transparentColor).drawRect(-10, -2, 20, 4);
            controlBackground.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(-8, -0.5, 16, 1, 0.5).drawRect(-0.1, -1, 0.2, 2);
            demo.field3.controlButton.graphics.beginFill(demo.selectedColor).drawCircle(0, 0, 1);
            // Controlled by x
            demo.field3.controlContainer.y = 35 * h / w;
            break;
        case "y":
            controlBackground.graphics.beginFill(demo.transparentColor).drawRect(-2, -10, 4, 20);
            controlBackground.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(-0.5, -8, 1, 16, 0.5).drawRect(-1, -0.1, 2, 0.2);
            demo.field3.controlButton.graphics.beginFill(demo.selectedColor).drawCircle(0, 0, 1);
            // Controlled by y
            demo.field3.controlContainer.x = 43;
            break;
        case "rotation":
            controlBackground.graphics.beginFill(demo.transparentColor).drawCircle(0, 0, 10);
            controlBackground.graphics.setStrokeStyle(1).beginStroke(demo.buttonBackgroundColor).drawCircle(0, 0, 6);
            demo.field3.controlButton.graphics.beginFill(demo.selectedColor).drawCircle(6, 0, 1.5);
            // Controlled by rotation
            if (demo.sharedRoom.character) demo.field3.controlButton.rotation = demo.sharedRoom.character.rotation;
            break;
        case "size":
            controlBackground.graphics.beginFill(demo.transparentColor).drawRect(-10, -2, 20, 4);
            controlBackground.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(-8, -0.5, 16, 1, 0.5);
            demo.field3.controlButton.graphics.beginFill(demo.selectedColor).drawCircle(0, 0, 1);
            // Controlled by x
            demo.field3.controlContainer.y = 35 * h / w;
            if (demo.sharedRoom.character) demo.field3.controlButton.x = ((demo.sharedRoom.character.children[1].x - 3) * 2) - 8;
            break;
        default:
            console.warn("drawField3: No drawing defined for control element", demo.controlling);
            break;
    }
    demo.field3.controlContainer.addChild(controlBackground);
    demo.field3.controlContainer.addChild(demo.field3.controlButton);
    demo.field3.controlContainer.on("mousedown", controlMovedHandler);
    demo.field3.controlContainer.on("pressmove", controlMovedHandler);
    demo.field3.controlContainer.on("pressup", controlMovedHandler);


    // Game

    // Container to compensate the character's movement
    demo.field3.offsetContainer = new createjs.Container();

    // Draw the field of the game
    var levelContainer = drawGameField();
    demo.field3.offsetContainer.addChild(levelContainer);

    // Add the synchronized character
    demo.field3.offsetContainer.addChild(demo.sharedRoom.character);

    scaledContainer.addChild(demo.field3.offsetContainer);
    scaledContainer.addChild(demo.field3.controlContainer);

    stage.addChild(background);
    stage.addChild(scaledContainer);
    stage.addChild(backToSelectControlsButton);
    drawFullscreenButton();
    stage.update();
};

/**
 * Draws the game field into a new Container
 * @returns {Container} The created Container for the game field
 */
var drawGameField = function () {
    var level = new createjs.Container();
    demo.field3.obstacles = [];

    // First all containers
    level.addChild(createObstacleCircle(true, 0, 0, 25)); // O Big start circle
    level.addChild(createObstacleRectangle(true, -5, 20, 10, 30, 0)); // | | Vertical
    level.addChild(createObstacleRectangle(true, -2, 40,  4, 40, 0)); // || Vertical
    level.addChild(createObstacleCircle(true, 0, 80, 5)); // o Small joint circle
    level.addChild(createObstacleRectangle(true,   0,    78.6,  30,  2.8, 0)); // - Small horizontal
    level.addChild(createObstacleRectangle(true,  20,    70,    60, 50,   0)); // ___| Big area
    level.addChild(createObstacleRectangle(true,  60,   107,   200, 15,  22)); // \\ Long diagonal tunnel's first part
    level.addChild(createObstacleRectangle(true, 245.4, 181.9, 125, 15,  39)); // \\ Long diagonal tunnel's second part
    level.addChild(createObstacleCircle(true, 400, 183, 100)); // O Giant arc
    level.addChild(createObstacleCircle(true, 300,  80,  50)); // O Medium arc
    level.addChild(createObstacleCircle(true, 260,  50,  25)); // C Bumper
    level.addChild(createObstacleCircle(true, 300,  20,  12.5)); // O Min arc to cycle around

    level.addChild(createObstacleCircle(true, 240, 190.5, 10)); // Patching a float calculation error at the tunnel's joint
    level.addChild(createObstacleRectangle(true, 325, 233, 30, 20,  39)); // Patching a float calculatoin error at the tunnel - Giant Arc joint
    level.addChild(createObstacleRectangle(true, 315, 125, 41, 15, -45)); // Patching a float calculatoin error at the Medium - Giant Arc joint
    level.addChild(createObstacleRectangle(true, 293,  29, 14,  4,   0)); // Patching a float calculatoin error at the cycle around circle joint

    // Then all other obstacles
    level.addChild(createObstacleRectangle(false, -2.2, 50,   -10,  10,  60)); // Slope for the smaller vertical left
    level.addChild(createObstacleRectangle(false,  2.2, 50,    10,  10, -60)); // Slope for the smaller vertical right
    level.addChild(createObstacleRectangle(false,  20,  81.7,   5,   5,  30)); // Entry slope for the Big area
    level.addChild(createObstacleRectangle(false,  20,  73.5,  10, -10, -45)); // Upper left clipping of the Big area
    level.addChild(createObstacleRectangle(false,  15,  84,    12,  40,   0)); // Shrink the lower left side of the Big area
    level.addChild(createObstacleRectangle(false,  30,  60,     3,  34,   0)); // | Vertical bar
    level.addChild(createObstacleRectangle(false,  30,  98,   30.75, 7,   0)); // -- Middle bar
    level.addChild(createObstacleRectangle(false,  25, 108,   200,  20,  19)); // \ Slope below the Middle bar, then lower bound of Long diagonal tunnel's first part
    level.addChild(createObstacleRectangle(false,  55,  88,     8,  20,  60)); // ^ Peak ontop of the Middle bar 1
    level.addChild(createObstacleRectangle(false,  55,  88,    11.5, 8,  60)); // ^ Peak ontop of the Middle bar 2
    level.addChild(createObstacleRectangle(false,  55,  70,    50, -25,  60)); // \ Slope in the upper right corner of the Big area
    level.addChild(createObstacleRectangle(false, 214, 173.1, 200,  20,  35)); // \ Lower bound of Long diagonal tunnel's second part
    level.addChild(createObstacleCircle(false, 400, 183, 90)); // 0 Inner Giant arc
    level.addChild(createObstacleCircle(false, 300,  79, 45)); // 0 Inner Medium arc
    level.addChild(createObstacleCircle(false, 260,  50, 20)); // c Inner Bumper
    level.addChild(createObstacleCircle(false, 300,  20,  9.5)); // 0 Inner Min arc to cycle around

    return level;
};

/**
 * Creates an obstacle with the shape of a circle and adds it to the demo.field3.obstacles Array
 * @param {boolean} isContainer Has the character to be inside or outside of the obstacle
 * @param {number} x The x-coordinate of the circle
 * @param {number} y The y-coordinate of the circle
 * @param {number} r The radius of the circle
 * @returns {Shape} The created obstacle
 */
var createObstacleCircle = function (isContainer, x, y, r) {
    var circ = new createjs.Shape();
    circ.graphics.beginFill(isContainer ? demo.noObstacleColor : demo.obstacleColor).drawCircle(0, 0, r);
    circ.isContainer = isContainer;
    circ.x = x;
    circ.y = y;
    circ.type = "circle";
    circ.r = r;

    // Store precomputed values
    if (isContainer) {
        circ.r2 = (r - 1) * (r - 1);
    } else {
        circ.r2 = (r + 1) * (r + 1);
    }

    demo.field3.obstacles.push(circ);
    return circ;
};


/**
 * Creates an obstacle with the shape of a rectangle and adds it to the demo.field3.obstacles Array
 * @param {boolean} isContainer Has the character to be inside or outside of the obstacle
 * @param {number} x The x-coordinate of the rectangle
 * @param {number} y The y-coordinate of the rectangle
 * @param {number} w The width of the rectangle
 * @param {number} h The height of the rectangle
 * @param {number} [rotation = 0] The rotation of the rectangle
 * @returns {Shape} The created obstacle
 */
var createObstacleRectangle = function (isContainer, x, y, w, h, rotation) {
    var rect = new createjs.Shape();
    rect.graphics.beginFill(isContainer ? demo.noObstacleColor : demo.obstacleColor).drawRect(0, 0, w, h);
    rect.isContainer = isContainer;
    rect.x = x;
    rect.y = y;
    rect.rotation = rotation || 0;
    rect.type = "rectangle";

    // Store precomputed values
    rect.rad = rect.rotation * degToRad;
    if (isContainer) {
        // Shrink the container to a smaller rectangle with a margin of the character's dot's radius
        rect.ax = rect.x + Math.cos((rect.rotation + 45) * degToRad) * sqrt2; // coordinates of the A corner
        rect.ay = rect.y + Math.sin((rect.rotation + 45) * degToRad) * sqrt2;
        rect.ab = [Math.cos(rect.rad) * (w - 2), Math.sin(rect.rad) * (w - 2)]; // Vectors in Rectangle A B
        rect.ad = [-Math.sin(rect.rad) * (h - 2), Math.cos(rect.rad) * (h - 2)]; //                      D C
    } else {
        // Extend the container to a bigger rectangle with a padding of the character's dot's radius
        rect.ax = rect.x - Math.cos((rect.rotation + 45) * degToRad) * sqrt2; // coordinates of the A corner
        rect.ay = rect.y - Math.sin((rect.rotation + 45) * degToRad) * sqrt2;
        rect.ab = [Math.cos(rect.rad) * (w + 2), Math.sin(rect.rad) * (w + 2)]; // Vectors in Rectangle A B
        rect.ad = [-Math.sin(rect.rad) * (h + 2), Math.cos(rect.rad) * (h + 2)]; //                      D C
    }
    rect.abab = dot(rect.ab, rect.ab); // Dot product of the two vectors
    rect.adad = dot(rect.ad, rect.ad);

    demo.field3.obstacles.push(rect);
    return rect;
};


/**
 * General storage object for the demo
 * Is not global, but accessible by all scripts
 */
var demo = {
    field1: {}, // Relevant for a single field, cleared after leaving the field
    field2: {},
    field3: {},
    nextRefresh: Infinity,
    refreshDelta: 0,
    refreshFunction: null,
    refreshInterval: Infinity,
    roomName: "",
    sharedRoom: null // Room object shared between all members
};

// Polling for refresh
demo.handleTick = function (event) {
    if (!event.paused) { // Ticker is not paused
        demo.refreshDelta += event.delta;
        demo.nextRefresh -= event.delta;
        if (demo.nextRefresh <= 0) {
            demo.nextRefresh += demo.refreshInterval;
            if (demo.nextRefresh < 0) demo.nextRefresh = 0;
            if (demo.refreshFunction) demo.refreshFunction(demo.refreshDelta);
            demo.refreshDelta = 0;
        }
    }
};
createjs.Ticker.addEventListener("tick", demo.handleTick);

/**
 * This customAction can be called to request the sharedRoom object from other room members
 * @param {string} requester The identifier of the client that requested the sharedRoom
 * @param {string} roomName The name of the sharedRoom's room
 */
var customActionGetSharedRoom = function (requester, roomName) {
    if (demo.sharedRoom && demo.roomName === roomName) { // Has a sharedRoom itself and the requester is a member of this room
        var sharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(demo.sharedRoom.sharedId);
        MTLG.distributedDisplays.communication.sendCustomAction(requester, "receiveSharedRoom", sharedObj.toSendables());
    }
};

/**
 * This customAction is called as a response to customActionGetSharedRoom and transmits the sharedRoom to the requester
 * @param {Sendable} sendableRoom The sendable representation of the sharedRoom
 */
var customActionReceiveSharedRoom = function (sendableRoom) {
    if (!demo.sharedRoom) { // Not already received from a different member
        // Get the sharedId from the associative list
        var sharedId;
        for (var i in sendableRoom) {
            if (sendableRoom[i].sharedId) {
                sharedId = sendableRoom[i].sharedId;
                break;
            }
        }

        // Convert the sharedRoom from its sendable and assign the wrapped object to demo.sharedRoom
        // The addSharedObjects action is used instead of SharedObject.fromSendable, to support more complex sendableRoom structures in the future
        MTLG.distributedDisplays.actions.addSharedObjects(sendableRoom);
        var sharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
        demo.sharedRoom = sharedObj.createJsObject;

        // Update start / join button
        if (demo.sharedRoom.inGame) {
            if (demo.field2.startButton) demo.field2.startButton.children[1].text = locText("field2_joinGameButton");
            if (demo.field2.joinGameNote) demo.field2.joinGameNote.visible = true;
        }
    }
};

/**
 * This custom function is called after receiving an update for a SharedObject, reacts to this
 * @param {object} action The executed action
 */
var customFunctionOnUpdate = function (action) {
    if (demo.sharedRoom && demo.sharedRoom.sharedId === action.sharedId) { // SharedRoom object has changed
        switch (action.propertyPath) {
            case "inGame":
                if (action.newValue && !action.oldValue) { // If a member started the game
                    if (demo.controlling) {
                        startGame(); // If a control is selected, start as well
                    } else {
                        // If no control is selected, update start / join button
                        if (demo.field2.startButton) demo.field2.startButton.children[1].text = locText("field2_joinGameButton");
                        if (demo.field2.joinGameNote) demo.field2.joinGameNote.visible = true;
                    }
                } else if (!action.newValue && action.oldValue) {
                    if (demo.field2.startButton) demo.field2.startButton.children[1].text = locText("field2_startButton");
                    if (demo.field2.joinGameNote) demo.field2.joinGameNote.visible = false;
                }
                break;

            case "debug.velocityX":
                if (demo.controlling === "x" && demo.field3.controlButton) demo.field3.controlButton.x = action.newValue;
                break;
            case "debug.velocityY":
                if (demo.controlling === "y" && demo.field3.controlButton) demo.field3.controlButton.y = action.newValue;
                break;
            case "debug.rotation":
                if (demo.controlling === "rotation" && demo.field3.controlButton) demo.field3.controlButton.rotation = action.newValue;
                break;
            case "debug.size":
                if (demo.controlling === "size" && demo.field3.controlButton) demo.field3.controlButton.x = action.newValue;
                break;
        }
    }
};

/**
 * Draws a button to toggle fullscreen
 */
var drawFullscreenButton = function () {
    var button = createButton(3 * demo.h, locText("general_fullscreenButton"));
    button.x = demo.h;
    button.y = demo.h;
    button.on("click", toggleFullscreen);
    stage.addChild(button);
};

/**
 * Toggles fullscreen
 */
var toggleFullscreen = function () {
    var isFullscreen = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
    var elem = document.body;
    if (!isFullscreen) {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
};


var degToRad = Math.PI / 180;
var radToDeg = 180 / Math.PI;
var sqrt2 = Math.sqrt(2);

/**
 * Returns a number whose value is limited to the given range.
 * @param {number} value The number to clamp
 * @param {number} min The lower boundary of the output range
 * @param {number} max The upper boundary of the output range
 * @returns {number} The clamped number in the range [min, max]
 */
var clamp = function (value, min, max) {
    return Math.min(Math.max(value, min), max);
};

/**
 * Creates a new button like element, inline text button
 * @param {number} h The height of the button, its width is calculated from the text width
 * @param {string} text The text to display on the button
 * @returns {Container} The created button element
 */
var createButton = function (h, text) {
    var buttonContainer = new createjs.Container();
    var textObj = new createjs.Text(text, demo.buttonFont, demo.textColor);
    textObj.textBaseline = 'middle';
    textObj.textAlign = 'center';
    var bounds = textObj.getBounds();
    textObj.x = (bounds.width + h) / 2; // Width of text + padding
    textObj.y = h / 2;

    var back = new createjs.Shape();
    back.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(0, 0, bounds.width + h, h, h / 10);

    buttonContainer.addChild(back);
    buttonContainer.addChild(textObj); // Text should be the second child
    return buttonContainer;
};

/**
 * Creates a new button like element, bigger button with image and text
 * @param {number} w The width of the button
 * @param {number} h The height of the button
 * @param {DisplayObject} image The Shape, Bitmap or other DisplayObject to use as central image
 * @param {string} text The text to display on the button#
 * @returns {Container} The created button element
 */
var createImageButton = function (w, h, image, text) {
    var buttonContainer = new createjs.Container();

    var textObj = new createjs.Text(text, demo.buttonFont, demo.textColor);
    textObj.textBaseline = 'alphabetical';
    textObj.textAlign = 'center';
    textObj.x = w / 2;
    textObj.y = h - demo.buttonMargin;

    var subText = new createjs.Text("", demo.buttonFont, demo.disabledColor);
    subText.textBaseline = 'bottom';
    subText.textAlign = 'center';
    subText.x = w / 2;
    subText.y = h;

    var back = new createjs.Shape();
    back.graphics.beginStroke(demo.disabledColor).beginFill(demo.transparentColor).drawRect(0, 0, w, h); // Background FFF fill is for the click handler

    image.x = w / 2;
    image.y = h / 3;

    buttonContainer.addChild(back);
    buttonContainer.addChild(textObj);
    buttonContainer.addChild(subText);
    buttonContainer.addChild(image);

    buttonContainer.changeState = function (color, subtext) {
        textObj.color = color;
        if (image.graphics._fill) image.graphics._fill.style = color;
        if (image.graphics._stroke) image.graphics._stroke.style = color;
        subText.text = subtext;
    };

    return buttonContainer;
};

/**
 * Creates a SharedObject for room information, it is no CreateJS object
 * @param {string} roomName The name of the room
 * @returns {object} The created shared room object, which is wrapped by a SharedObject
 */
var createSharedRoomObject = function (roomName) {
    var character = new createjs.Container();
    var dot1 = new createjs.Shape();
    dot1.graphics.beginFill(demo.characterColor).drawCircle(0, 0, 1);
    dot1.x = -7;
    var dot2 = new createjs.Shape();
    dot2.graphics.beginFill(demo.characterColor).drawCircle(0, 0, 1);
    dot2.x = 7;
    character.addChild(dot1);
    character.addChild(dot2);
    // Trigger an update of the _instructions Array
    dot1.graphics.instructions;
    dot2.graphics.instructions;

    var roomObj = {
        controls: { // The assigned controls
            x: false, // False indicates that the control is free, a client id indicates who controls the element
            y: false,
            rotation: false,
            size: false
        },
        character: character, // The synchronized character itself
        debug: { // For remote control
            velocityX: 0,
            velocityY: 0,
            rotation: 0,
            size: 7
        },
        gameOver: false, // Lost
        inGame: false, // Is the group currently playing a level
        name: roomName // The name of the room
    };
    // Wrap the roomObj with a SharedObject, but postpone the initialization
    var sharedObj = new MTLG.distributedDisplays.SharedObject(roomObj, 0, false, true);
    // Configure the properties to synchronize
    sharedObj.syncProperties = {
        controls: {
            send: true, receive: true, className: "Object",
            subProperties: {
                x:        { send: true, receive: true },
                y:        { send: true, receive: true },
                rotation: { send: true, receive: true },
                size:     { send: true, receive: true }
            }
        },
        character: { send: true, receive: true, className: "SharedObject" },
        debug: {
            send: true, receive: true, className: "Object",
            subProperties: {
                velocityX: { send: true, receive: true },
                velocityY: { send: true, receive: true },
                rotation:  { send: true, receive: true },
                size:      { send: true, receive: true }
            }
        },
        gameOver: { send: true, receive: true },
        inGame:   { send: true, receive: true },
        name:     { send: true, receive: true }
    };
    // Initialize the sharedObj to detect changes
    sharedObj.init();
    return roomObj;
};

/**
 * Calculates the dot product between the two vectors (Arrays) a and b
 * @param {Array.<number>} a The first vector of the dot product
 * @param {Array.<number>} b The second vector of the dot product
 * @returns {number} The dot product between the two vectors (Arrays) a and b
 */
var dot = function (a, b) {
    return (a[0] * b[0]) + (a[1] * b[1]);
};

/**
 * Returns the localized representation of the given text template
 * @param {string} text The text template ot localize
 * @return {string} The resulting text
 */
var locText = function (text) {
    return (l(text));
};
