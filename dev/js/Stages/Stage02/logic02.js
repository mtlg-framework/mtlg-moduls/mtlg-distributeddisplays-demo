/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2017-11-13T16:36:48+01:00
 */



/**
 * Switches to the next field: Game
 */
var startGame = function () {
    if (demo.controlling) {
        demo.refreshFunction = null;
        demo.refreshInterval = Infinity;
        demo.field2 = {};
        demo.sharedRoom.inGame = true;
        feld3_init();
    }
};
