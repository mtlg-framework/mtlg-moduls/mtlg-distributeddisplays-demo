/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2017-11-13T16:38:22+01:00
 */



/**
 * Initialize the second field: Control selection
 */
var feld2_init = function () {
    stage.removeAllChildren();
    demo.members = {}; // Cache for the reversed demo.sharedRoom
    demo.refreshFunction = refreshMemberList;
    demo.refreshInterval = 300;//ms
    demo.nextRefresh = demo.refreshInterval;

    drawField2();
};

/**
 * Draw the second field: Control selection
 */
var drawField2 = function () {
    var w = demo.w;
    var h = demo.h;


    // Background
    var background = new createjs.Shape();
    background.graphics.beginFill("#FFF").drawRect(0, 0, 100 * w, 100 * h);

    // Text
    var title = new createjs.Text(locText("field2_title"), demo.titleFont, demo.textColor);
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 50 * w;
    title.y = 10 * h;

    var id = MTLG.distributedDisplays.rooms.getClientId();
    var subTitle = new createjs.Text(locText("field2_subTitle1, " + id + ", field2_subTitle2 " + demo.roomName), demo.normalFont, demo.textColor);
    subTitle.textBaseline = 'alphabetic';
    subTitle.textAlign = 'center';
    subTitle.x = 50 * w;
    subTitle.y = 14 * h;

    // Members

    // Headline for the list of current members
    var currentMembersText = new createjs.Text(locText("field2_currentMembers: "), demo.normalFont, demo.textColor);
    currentMembersText.textBaseline = 'alphabetic';
    currentMembersText.x = 5 * w;
    currentMembersText.y = 21 * h;

    // Member list
    demo.field2.memberList = new createjs.Container();
    demo.field2.memberList.x = 5 * w;
    demo.field2.memberList.y = 25 * h;

    // Controls

    // Container
    var controlSelection = new createjs.Container();
    controlSelection.x = 10 * w;
    controlSelection.y = 60 * h;

    // Images for the buttons, the width of all is 12 * w
    var xImage = new createjs.Shape();
    xImage.graphics.beginFill(demo.textColor).drawRect(-4 * w, -0.5 * w, 8 * w, 1 * w).drawPolyStar(-4 * w, 0, 2 * w, 3, 0, 180).drawPolyStar(4 * w, 0, 2 * w, 3, 0, 0);
    var yImage = new createjs.Shape();
    yImage.graphics.beginFill(demo.textColor).drawRect(-0.75 * h, -6 * h, 1.5 * h, 12 * h).drawPolyStar(0, -6 * h, 3 * h, 3, 0, -90).drawPolyStar(0, 6 * h, 3 * h, 3, 0, 90);
    var piFrac = Math.PI / 6; // One twelth of a full circle
    var rotationImage = new createjs.Shape();
    rotationImage.graphics.setStrokeStyle(1 * w).beginStroke(demo.textColor).arc(0, 0, 4 * w, -5.5 * piFrac, 2 * piFrac);
    rotationImage.graphics.beginFill(demo.textColor).drawPolyStar(Math.cos(   2 * piFrac) * 4 * w, Math.sin(   2 * piFrac) * 4 * w, 0.5 * w, 3, 0, 143)
                                                    .drawPolyStar(Math.cos(-5.5 * piFrac) * 4 * w, Math.sin(-5.5 * piFrac) * 4 * w, 0.5 * w, 3, 0, 117);
    var sizeImage = new createjs.Shape();
    sizeImage.graphics.beginFill(demo.textColor).drawRect(-4 * w, -0.5 * w, 3 * w, 1 * w).drawRect(1 * w, -0.5 * w, 3 * w, 1 * w).drawPolyStar(-4 * w, 0, 1.5 * w, 3, 0, 180)
                                                                                                                                  .drawPolyStar( 4 * w, 0, 1.5 * w, 3, 0, 0);
    sizeImage.rotation = -45;

    // Buttons
    demo.field2.controlButtons = {};
    demo.field2.controlButtons.x = createImageButton(19 * w, 30 * h, xImage, locText("field2_memberControls_x"));
    demo.field2.controlButtons.y = createImageButton(19 * w, 30 * h, yImage, locText("field2_memberControls_y"));
    demo.field2.controlButtons.rotation = createImageButton(19 * w, 30 * h, rotationImage, locText("field2_memberControls_rotation"));
    demo.field2.controlButtons.size = createImageButton(19 * w, 30 * h, sizeImage, locText("field2_memberControls_size"));
    demo.field2.controlButtons.x.x = 0.5 * w;
    demo.field2.controlButtons.y.x = 20.5 * w;
    demo.field2.controlButtons.rotation.x = 40.5 * w;
    demo.field2.controlButtons.size.x = 60.5 * w;
    demo.field2.controlButtons.x.on('click', controlClickedHandler, null, false, "x");
    demo.field2.controlButtons.y.on('click', controlClickedHandler, null, false, "y");
    demo.field2.controlButtons.rotation.on('click', controlClickedHandler, null, false, "rotation");
    demo.field2.controlButtons.size.on('click', controlClickedHandler, null, false, "size");

    controlSelection.addChild(demo.field2.controlButtons.x);
    controlSelection.addChild(demo.field2.controlButtons.y);
    controlSelection.addChild(demo.field2.controlButtons.rotation);
    controlSelection.addChild(demo.field2.controlButtons.size);

    // Other buttons

    // Start the game
    var text = (demo.sharedRoom && demo.sharedRoom.inGame) ? "field2_joinGameButton" : "field2_startButton";
    demo.field2.startButton = createButton(6 * h, locText(text));
    demo.field2.startButton.x = 90 * w - demo.field2.startButton.getBounds().width - 6 * h;
    demo.field2.startButton.y = 50 * h;
    demo.field2.startButton.on('click', startGame);

    // Join game note
    demo.field2.joinGameNote = new createjs.Text(locText("field2_joinGameNote"), demo.normalFont, demo.textColor);
    demo.field2.joinGameNote.textBaseline = 'alphabetic';
    demo.field2.joinGameNote.textAlign = 'right';
    demo.field2.joinGameNote.x = 90 * w;
    demo.field2.joinGameNote.y = 44 * h;
    demo.field2.joinGameNote.visible = demo.sharedRoom && demo.sharedRoom.inGame;

    // Leave the room
    var leaveRoomButton = createButton(3 * h, locText("field2_leaveRoomButton"));
    leaveRoomButton.x = 90 * w - leaveRoomButton.getBounds().width - 3 * h;
    leaveRoomButton.y = 19 * h;
    leaveRoomButton.on('click', leaveRoom);

    stage.addChild(background);
    stage.addChild(title);
    stage.addChild(subTitle);
    stage.addChild(currentMembersText);
    stage.addChild(demo.field2.memberList);
    stage.addChild(controlSelection);
    stage.addChild(demo.field2.startButton);
    stage.addChild(demo.field2.joinGameNote);
    stage.addChild(leaveRoomButton);
    drawFullscreenButton();
    stage.update();
};


/**
 * Switches to the previous field: Control selection
 */
var backToSelectControls = function () {
    demo.refreshFunction = null;
    demo.refreshInterval = Infinity;
    demo.field3 = {};
    feld2_init();
};

/**
 * Checks if the character has collided with something
 */
var checkGameOver = function () {
    // Positions of the two points
    var dx = Math.cos(demo.sharedRoom.character.rotation * degToRad) * demo.sharedRoom.character.children[1].x; // Positive child
    var dy = Math.sin(demo.sharedRoom.character.rotation * degToRad) * demo.sharedRoom.character.children[1].x; // Positive child
    var x1 = demo.sharedRoom.character.x + dx;
    var x2 = demo.sharedRoom.character.x - dx;
    var y1 = demo.sharedRoom.character.y + dy;
    var y2 = demo.sharedRoom.character.y - dy;

    // Check if the character is indide of any container
    var inContainer1 = false;
    var inContainer2 = false;
    var i = 0;
    while (i < demo.field3.obstacles.length && demo.field3.obstacles[i].isContainer && !(inContainer1 && inContainer2)) {
        if (!inContainer1 && checkDotInObstacle(x1, y1, demo.field3.obstacles[i])) {
            inContainer1 = true;
        }
        if (!inContainer2 && checkDotInObstacle(x2, y2, demo.field3.obstacles[i])) {
            inContainer2 = true;
        }
        i++;
    }
    if (!(inContainer1 && inContainer2)) return gameOver();

    // Check if touching any not container obstacle
    while (i < demo.field3.obstacles.length) {
        if (!demo.field3.obstacles[i].isContainer && (checkDotInObstacle(x1, y1, demo.field3.obstacles[i]) || checkDotInObstacle(x2, y2, demo.field3.obstacles[i]))) {
            return gameOver();
        }
        i++;
    }
    demo.sharedRoom.gameOver = false;
};

/**
 * If the obstacle is a  container, it returns whether the character's dot is completely inside the given obstacle
 * If the obstacle is no container, it returns whether the character's dot touches the given obstacle
 * @param {number} x The x-coordinate of the dot
 * @param {number} y The y-coordinate of the dot
 * @param {Shape} obstacle The obstacle container to check
 * @returns {boolean} True, if the dot is completely inside the container or touches the obstacle
 */
var checkDotInObstacle = function (x, y, obstacle) {
    switch (obstacle.type) {
        case "circle":
            return ((obstacle.x - x) * (obstacle.x - x)) + ((obstacle.y - y) * (obstacle.y - y)) < obstacle.r2;
            break;
        case "rectangle":
            var apab = dot([x - obstacle.ax, y - obstacle.ay], obstacle.ab);
            var apad = dot([x - obstacle.ax, y - obstacle.ay], obstacle.ad);
            return 0 < apab && apab < obstacle.abab && 0 < apad && apad < obstacle.adad;
            break;
        default:
            console.warn("checkDotInObstacle: No check for the obstacle type", obstacle.type, "defined.");
            break;
    }
};

/**
 * Handler for the movement of the control button
 * @param {object} event The click event
 */
var controlMovedHandler = function (event) {
    if (!demo.sharedRoom.gameOver) {
        if (event.type === "pressup") {
            // Snap back to 0 on pressup
            switch (demo.controlling) {
                case "x":
                    demo.field3.controlButton.x = 0;
                    demo.field3.velocityX = 0;
                    break;
                case "y":
                    demo.field3.controlButton.y = 0;
                    demo.field3.velocityY = 0;
                    break;
            }
        } else {
            var relativePoint = demo.field3.controlContainer.globalToLocal(event.stageX, event.stageY);
            switch (demo.controlling) {
                case "x":
                    var newX = clamp(relativePoint.x, -8, 8); // Boundary of the slider
                    demo.field3.controlButton.x = newX;
                    demo.field3.velocityX = newX * demo.field3.maxVelocity;
                    break;
                case "y":
                    var newY = clamp(relativePoint.y, -8, 8); // Boundary of the slider
                    demo.field3.controlButton.y = newY;
                    demo.field3.velocityY = newY * demo.field3.maxVelocity;
                    break;
                case "rotation":
                    var newRotation = Math.atan2(relativePoint.y, relativePoint.x) * radToDeg; // Convert to angle in radians and then to degree, mind atan2(y, x) not x,y
                    demo.field3.controlButton.rotation = newRotation;
                    demo.sharedRoom.character.rotation = newRotation;
                    break;
                case "size":
                    var newSize = clamp(relativePoint.x, -8, 8); // Boundary of the slider
                    demo.field3.controlButton.x = newSize;
                    newSize = ((newSize + 8) / 2) + 3; // Transform: [-8,8], [0,16], [0,8], [3,11] the outer size is then in [8, 24]
                    demo.sharedRoom.character.children[0].x = -newSize;
                    demo.sharedRoom.character.children[1].x = newSize;
                    break;
            }
        }
    }
};

/**
 * Lost the game
 */
var gameOver = function () {
    if (!demo.sharedRoom.gameOver) {
        demo.sharedRoom.gameOver = true;
        demo.field3.velocityX = 0;
        demo.field3.velocityY = 0;
        swal({
            type: 'error',
            title: locText("field3_gameOverTitle"),
            text: locText("field3_gameOverText"),
            confirmButtonText: locText("field3_gameOverConfirm"),
            allowEnterKey: true,
            allowEscapeKey: demo.debug === true, // Keep going with escape key
            allowOutsideClick: false,
            showCancelButton: false
        }).then(function () {
            // Confirmed to restart
            demo.sharedRoom.character.x = 0;
            demo.sharedRoom.character.y = 0;
            demo.sharedRoom.character.rotation = 0;
            demo.sharedRoom.character.children[0].x = -7;
            demo.sharedRoom.character.children[1].x = 7;
            demo.sharedRoom.gameOver = false;
            if (demo.field3.controlButton) {
                demo.field3.controlButton.x = 0;
                demo.field3.controlButton.y = 0;
                demo.field3.controlButton.rotation = 0;
            }
        }, function (dismiss) {
            // Dismissed the popup, keep playing, only possible if debug === true
            // noop
        });
    }
};

/**
 * Moves the character according to the current velocity
 * @param {number} delta Time since last frame
 */
var moveCharacter = function (delta) {
    if (demo.debug && demo.keyMaster) {
        var velX = 0,
            velY = 0,
            rot = demo.sharedRoom.character.rotation,
            size = demo.sharedRoom.character.children[1].x;

        if (demo.keyMap.ArrowRight) velX += 0.2;
        if (demo.keyMap.ArrowLeft) velX -= 0.2;
        if (velX !== 0) demo.sharedRoom.character.x += velX;

        if (demo.keyMap.ArrowUp) velY -= 0.2;
        if (demo.keyMap.ArrowDown) velY += 0.2;
        if (velY !== 0) demo.sharedRoom.character.y += velY;

        if (demo.keyMap.a) rot -= 2;
        if (demo.keyMap.d) rot += 2;
        if (rot !== demo.sharedRoom.character.rotation) demo.sharedRoom.character.rotation = rot;

        if (demo.keyMap.w) {
            if (size <= 10.8) {
                size += 0.2;
            } else {
                size = 11;
            }
            demo.sharedRoom.character.children[0].x = -size;
            demo.sharedRoom.character.children[1].x = size;
        }
        if (demo.keyMap.s) {
            if (size >= 3.2) {
                size -= 0.2;
            } else {
                size = 3;
            }
            demo.sharedRoom.character.children[0].x = -size;
            demo.sharedRoom.character.children[1].x = size;
        }

        // Update remote control elements
        demo.sharedRoom.debug.velocityX = velX * 20;
        demo.sharedRoom.debug.velocityY = velY * 20;
        demo.sharedRoom.debug.rotation = rot;
        demo.sharedRoom.debug.size = ((size - 3) * 2) - 8;

        // Update own control element
        if (demo.controlling && demo.field3.controlButton) {
            if (demo.controlling === "x") demo.field3.controlButton.x = velX * 20;
            if (demo.controlling === "y") demo.field3.controlButton.y = velY * 20;
            if (demo.controlling === "rotation") demo.field3.controlButton.rotation = rot;
            if (demo.controlling === "size") demo.field3.controlButton.x = ((size - 3) * 2) - 8;
        }
    }

    if (demo.controlling === "x" && demo.field3.velocityX !== 0) demo.sharedRoom.character.x += demo.field3.velocityX * delta;
    if (demo.controlling === "y" && demo.field3.velocityY !== 0) demo.sharedRoom.character.y += demo.field3.velocityY * delta;
    // Move the offset container
    updateOffsetContainer(delta);
    checkGameOver();
};

/**
 * Update the offset to compensate the characters movement
 * @param {number} delta Time since last frame
 */
var updateOffsetContainer = function (delta) {
    if (demo.field3.offsetContainer) {
        var diffX = demo.field3.offsetContainer.x + demo.sharedRoom.character.x;
        var diffY = demo.field3.offsetContainer.y + demo.sharedRoom.character.y;
        // If more than 20 percent off of the center, update the offsetContainer: difference above 20, from which a twentyfith is compensated each frame
        if (Math.abs(diffX) > 10) demo.field3.offsetContainer.x -= (diffX - (Math.sign(diffX) * 10)) / 25;
        if (Math.abs(diffY) > 10) demo.field3.offsetContainer.y -= (diffY - (Math.sign(diffY) * 10)) / 25;
    }
};
