/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2017-11-13T17:07:35+01:00
 */



MTLG.lang.define({
  'en': {
    'demo_game': 'AAAA',
    'einSpieler': 'One Player',
    'zweiSpieler': 'Two Players',
    'titel-Login': 'Type in User and password',
    'temp_feedback': 'Your needed time in milliseconds is: ',
    'feedback_text': 'ms needed',
    'Menuetext_Zurueck': 'Back to game',
    'Menuetext_ZumHauptmenue': 'Main Menu',
    'NeuesSpiel': 'Play again',

    'field1_addRoomErrorTitle': 'Room creation failed',
    'field1_addRoomPlaceholder': 'Room name',
    'field1_addRoomQuestion': 'Please enter the name of the room to add:',
    'field1_addRoomTitle': 'Add a new room',
    'field1_emptyInput': 'You need to write something!',
    'field1_joinRoomButton': 'JOIN',
    'field1_joinRoomErrorTitle': 'Failed to join room',
    'field1_newRoomButton': 'NEW ROOM',
    'field1_refreshButton': 'REFRESH',
    'field1_title': 'Join an existing room or create a new one.',

    'field2_currentMembers': 'Current members',
    'field2_joinGameButton': 'JOIN',
    'field2_joinGameNote': 'This group is already playing, you can join them or leave the room.',
    'field2_leaveRoomButton': 'LEAVE ROOM',
    'field1_leaveRoomErrorTitle': 'Failed to leave room',
    'field2_memberControls_rotation': 'Rotation',
    'field2_memberControls_size': 'Size',
    'field2_memberControls_true': 'Nothing',
    'field2_memberControls_x': 'X-axis',
    'field2_memberControls_y': 'Y-axis',
    'field2_startButton': 'START',
    'field2_subTitle1': 'You',
    'field2_subTitle2': 'are a member of the room',
    'field2_title': 'Choose a control and start the game.',

    'field3_backToSelectControlsButton': 'SELECT CONTROL',
    'field3_gameOverTitle': 'Game Over',
    'field3_gameOverText': 'You have touched an obstacle',
    'field3_gameOverConfirm': 'START OVER',

    'general_fullscreenButton': 'FULLSCREEN'
  },
  'de': {
    'demo_game':'OOOO',
    'einSpieler': 'Ein Spieler',
    'zweiSpieler': 'Zwei Spieler',
    'titel-Login': 'Bitte Namen und Passwort eingeben',
    'temp_feedback': 'Deine benötigte Zeit (in ms) beträgt: ',
    'feedback_text': 'ms gebraucht',
    'Menuetext_Zurueck': 'Zurück zum Spiel',
    'Menuetext_ZumHauptmenue': 'Hauptmenü',
    'NeuesSpiel': 'Erneut spielen',

    'field1_addRoomErrorTitle': 'Raum konnte nicht erstellt werden',
    'field1_addRoomPlaceholder': 'Raum Name',
    'field1_addRoomQuestion': 'Bitte den Namen des zu erstellenden Raumes eingeben:',
    'field1_addRoomTitle': 'Neuen Raum erstellen',
    'field1_emptyInput': 'Das feld darf nicht leer sein!',
    'field1_joinRoomButton': 'BEITRETEN',
    'field1_joinRoomErrorTitle': 'Dem Raum konnte nicht beigetreten werden',
    'field1_refreshButton': 'AKTUALISIEREN',
    'field1_newRoomButton': 'RAUM HINZUFÜGEN',
    'field1_title': 'Trete einem Raum bei oder erstelle einen neuen.',

    'field2_currentMembers': 'Derzeitige Mitglieder',
    'field2_joinGameButton': 'SPIELEN',
    'field2_joinGameNote': 'Diese Gruppe spielt bereits, du kannst beitreten oder den Raum verlassen.',
    'field2_leaveRoomButton': 'RAUM VERLASSEN',
    'field1_leaveRoomErrorTitle': 'Der Raum konnte nicht verlassen werden',
    'field2_memberControls_rotation': 'Rotation',
    'field2_memberControls_size': 'Größe',
    'field2_memberControls_true': 'Nichts',
    'field2_memberControls_x': 'X-Achse',
    'field2_memberControls_y': 'Y-Achse',
    'field2_startButton': 'STARTEN',
    'field2_subTitle1': 'Du',
    'field2_subTitle2': 'bist ein Mitglied des Raumes',
    'field2_title': 'Wähle eine Steuerung und starte das Spiel.',

    'field3_backToSelectControlsButton': 'STEUERUNG WÄHLEN',
    'field3_gameOverTitle': 'Das Spiel ist aus',
    'field3_gameOverText': 'Du hast ein Hindernis berührt',
    'field3_gameOverConfirm': 'NEUSTART',

    'general_fullscreenButton': 'VOLLBILD'
  }
});
